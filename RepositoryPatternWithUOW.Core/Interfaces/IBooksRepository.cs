﻿using RepositoryPatternWithUOW.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryPatternWithUOW.Core.Interfaces
{
    public interface IBooksRepository : IBaseRepository<Book>
    {

        // we write this method only to define how to make special repository to specific model
        // this happens when there is some methods to be apply on this model only 
        // so we made this specific repository and inherit from the base repository to use the base repository methods
        // and add our special methods
        IEnumerable<Book> SpecialMethod();
    }
}
